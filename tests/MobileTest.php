<?php

namespace Tests;

use App\FooCarrier;
use App\Mobile;
use App\Services\ContactService;
use App\Interfaces\CarrierInterface;
use App\SMS;
use Mockery as m;
use PHPUnit\Framework\TestCase;

class MobileTest extends TestCase
{
	
	/** @test */
	public function it_returns_null_when_name_empty()
	{
        $provider = m::mock('App\Interfaces\CarrierInterface');

        $mobile = new Mobile($provider);

		$this->assertNull($mobile->makeCallByName(''));
	}


	/** @test */
	public function it_check_passing_a_valid_contact()
	{
        $provider = new FooCarrier();
        $mobile = new Mobile($provider);
		$this->assertNotNull($mobile->makeCallByName('foo'));
	}

    /** @test */
    public function it_check_when_the_contact_is_not_found()
    {
        $service = m::mock('ContactService');
        $service->shouldReceive('findByName')
                ->with("not_found")
                ->andReturn(null);
        $contact = $service->findByName('not_found');
        $this->assertNull($contact);
    }

	/** @test */
    public function it_check_validateNumber_from_contact_service_ok()
    {
        $number = "996666765";
        $service = m::mock('ContactService');
        $service->shouldReceive('validateNumber')
            ->with($number)
            ->andReturn(true);
        $isValid = $service->validateNumber($number);
        $this->assertTrue($isValid);
    }

	/** @test */
    public function it_check_validateNumber_from_contact_service_fail()
    {
        $number = "hola996666765";
        $service = m::mock('ContactService');
        $service->shouldReceive('validateNumber')
            ->with($number)
            ->andReturn(true);
        $isValid = $service->validateNumber($number);
        $this->assertNotTrue($isValid);
    }

}
