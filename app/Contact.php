<?php

namespace App;


class Contact
{
	protected $number;

	function __construct()
	{
		# code...
	}

    /**
     * @param mixed $number
     */
    public function setNumber($number): void
    {
        $this->number = $number;
    }

    /**
     * @return mixed
     */
    public function getNumber()
    {
        return $this->number;
    }
}