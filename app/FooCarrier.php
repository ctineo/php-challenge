<?php


namespace App;


use App\Interfaces\CarrierInterface;

class FooCarrier implements CarrierInterface
{

    public function dialContact(Contact $contact)
    {
       echo "dialing";
    }

    public function makeCall(): Call
    {
        return new Call();
    }

    public function sendMessage($text): SMS
    {
        return new SMS($text);
    }
}