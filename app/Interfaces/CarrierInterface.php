<?php

namespace App\Interfaces;

use App\Call;
use App\SMS;
use App\Contact;


interface CarrierInterface
{
	
	public function dialContact(Contact $contact);

    public function makeCall(): Call;

    public function sendMessage($text): SMS;
}