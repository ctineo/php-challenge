<?php

namespace App\Services;

use App\Contact;


class ContactService
{
	
	public static function findByName($name): ?Contact
	{
        if($name == "not_found") return null; // simulate empty resultSet
	    return (empty($name))?null:new Contact();
	}

	public static function validateNumber(string $number): bool
	{
        return preg_match("/^[0-9]{3}-[0-9]{4}-[0-9]{4}$/", $number);
	}
}